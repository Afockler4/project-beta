from django.db import models
from django.urls import reverse
from datetime import datetime


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=500, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    ispurchased = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.pk})


class ServiceAppointment(models.Model):
    status_choices = (
        ("upcoming", "upcoming"),
        ("completed", "completed"),
        ("canceled", "canceled"),
    )

    owner_name = models.CharField(max_length=100)
    datetime_of_appointment = models.DateTimeField(default=datetime.now, blank=False)
    reason_for_appointment = models.TextField()
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.DO_NOTHING,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.DO_NOTHING,
        default=None,
    )
    status = models.CharField(max_length=10, choices=status_choices, default="upcoming")
    vip_status = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})

    def complete(self):
        self.status = "completed"
        self.save()

    def cancel(self):
        self.status = "canceled"
        self.save()

    def __str__(self):
        return f'{self.owner_name}\'s appointment for {self.reason_for_appointment} on {self.datetime_of_appointment}'


