from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


from .models import (AutomobileVO, Technician, ServiceAppointment,
)

from .encoders import (AutomobileVOEncoder, TechnicianEncoder, ServiceAppointmentEncoder,
    ServiceAppointmentListEncoder,
)

@require_http_methods(["GET"])
def api_automobileVO(request):
    if request.method == "GET":
        automobileVO = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobileVO": automobileVO},
            encoder=AutomobileVOEncoder
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            technician_id = content["technician"]
            technician = Technician.objects.get(employee_number=technician_id)
            content["technician"] = technician

            auto_vin = content["automobile"]
            automobile = AutomobileVO.objects.filter(vin=auto_vin).first()
            content["automobile"] = automobile

            if automobile.ispurchased is True:
                content["vip_status"] = True

            appointments = ServiceAppointment.objects.create(**content)
            return JsonResponse(
                appointments,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": "Failed to make a Service Appointment. Try again."}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, pk):
    try:

        if request.method == "GET":
            appointment = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            appointment = ServiceAppointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                {"appointments": appointment},
                encoder=ServiceAppointmentEncoder,
                safe=False
            )
        else:
            content = json.loads(request.body)
            appointment = ServiceAppointment.objects.get(id=pk)
            technician = Technician.objects.get(employee_number=content["technician"])
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
            appointment.owner_name = content["owner_name"]
            appointment.datetime_of_appointment = content["datetime_of_appointment"]
            appointment.reason_for_appointment = content["reason_for_appointment"]
            appointment.technician = technician
            appointment.automobile = content["automobile"]
            appointment.status = content["status"]
            appointment.save()

            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )


    except ServiceAppointment.DoesNotExist:
        return JsonResponse(
            {"message": "The Appointment ID given isn't valid. Try again."}
        )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Error creating a Technician"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_technician(request, pk):
    try:
        technician = Technician.objects.get(id=pk)
        if request.method == "GET":
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        else:
            content = json.loads(request.body)
            fields = ["name", "employee_number"]

            for field in fields:
                if field in content:
                    setattr(technician, field, content[field])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
    except Technician.DoesNotExist:
        response = JsonResponse({"message": "Technician can't be found with this ID"})
        response.status_code = 404
        return response




