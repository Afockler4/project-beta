from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import SalesPerson, PotentialCustomer, SalesRecord, AutomobileVO

from .encoders import (
    AutomobileVOEncoder,
    SalesPersonEncoder,
    PotentialCustomerEncoder,
    SalesRecordEncoder,
)


@require_http_methods(["GET"])
def api_automobileVOs(request):
    if request.method == "GET":
        automobileVO = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobileVO": automobileVO},
            encoder=AutomobileVOEncoder,
        )


@require_http_methods(["GET", "POST"])
def api_sales_people(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people},
            encoder=SalesPersonEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the Sales person."}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sales_person(request, pk):
    if request.method == "GET":
        try:
            model = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                model,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Not a valid Sales person."})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
            sales_person = SalesPerson.objects.get(id=pk)
            print(sales_person)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(id=pk)
            sales_person.name = content["name"]
            sales_person.employee_id = content["employee_id"]
            sales_person.save()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Sales person does not exist."}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_potential_customers(request):
    if request.method == "GET":
        potential_customers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"potential_customers": potential_customers},
            encoder=PotentialCustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            potential_customer = PotentialCustomer.objects.create(**content)
            return JsonResponse(
                potential_customer,
                encoder=PotentialCustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the Customer."}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_potential_customer(request, pk):
    if request.method == "GET":
        try:
            model = PotentialCustomer.objects.get(id=pk)
            return JsonResponse(
                model,
                encoder=PotentialCustomerEncoder,
                safe=False
            )
        except PotentialCustomer.DoesNotExist:
            response = JsonResponse({"message": "Not a valid Customer."})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            potential_customer = PotentialCustomer.objects.get(id=pk)
            potential_customer.delete()
            return JsonResponse(
                potential_customer,
                encoder=PotentialCustomerEncoder,
                safe=False,
            )
        except PotentialCustomer.DoesNotExist:
            response = JsonResponse({"message": "Not a valid Customer."})
            response.status_code = 400
            return response
    else:
        try:
            content = json.loads(request.body)
            potential_customer = PotentialCustomer.objects.get(id=pk)
            potential_customer.name = content["name"]
            potential_customer.address = content["address"]
            potential_customer.phone_number = content["phone_number"]
            potential_customer.save()
            return JsonResponse(
                potential_customer,
                encoder=PotentialCustomerEncoder,
                safe=False
            )
        except PotentialCustomer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist."}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_sales_records(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
            sales_person = SalesPerson.objects.get(pk=content["sales_person"])
            content["sales_person"] = sales_person
            customer = PotentialCustomer.objects.get(pk=content["customer"])
            content["customer"] = customer
            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {"message": "Could not create the sales record"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sales_record(request, pk):
    if request.method == "GET":
        try:
            sales_record = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "No sale available."})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            sales_record = SalesRecord.objects.get(id=pk)
            sales_record.delete()
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            sales_record = SalesRecord.objects.get(id=pk)
            sales_record.price = content["price"]
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
            sales_record.automobile = content["automobile"]
            sales_person = SalesPerson.objects.get(pk=content["sales_person"])
            content["sales_person"] = sales_person
            sales_record.sales_person = content["sales_person"]
            customer = PotentialCustomer.objects.get(pk=content["customer"])
            content["customer"] = customer
            sales_record.customer = content["customer"]
            sales_record.save()
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse(
                {"message": "Could not update the sales record"}
            )
            response.status_code = 400
            return response




