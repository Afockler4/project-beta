import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function Customer() {
  useEffect(() => {
    getData();
  }, []);

  const [customers, setCustomers] = useState([]);

  const [name, setCustomerName] = useState("");

  const [phone_number, setPhoneNumber] = useState();

  const [address, setAddress] = useState();

  const [modal, showModal] = useState(false);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.potential_customers);
    }
  };

  const handleCreateCustomer = async () => {
    const data = { name, phone_number, address };
    const customerNameUrl = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(customerNameUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
    closeModal();
  };

  const handleChangeCustomerName = (e) => {
    setCustomerName(e.target.value);
  };

  const handleChangePhoneNumber = (e) => {
    setPhoneNumber(e.target.value);
  };

  const handleChangeAddress = (e) => {
    setAddress(e.target.value);
  };

  const closeModal = () => {
    showModal(false);
  };

  const handleDeleteCustomer = async (id) => {
    const data = { id };
    const customerNameUrl = `http://localhost:8090/api/customers/${id}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(customerNameUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div className="otherpagescontainer">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Customers</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Address</th>
              <th>Phone Number</th>
              <th>
                <i
                  onClick={() => {
                    showModal(true);
                  }}
                  className="fas fa-plus-square"
                ></i>
              </th>
            </tr>
          </thead>
          <tbody>
            {customers.map((customer) => {
              return (
                <tr key={customer.id}>
                  <td>{customer.name}</td>
                  <td>{customer.address}</td>
                  <td>{customer.phone_number}</td>
                  <td>
                    <i
                      onClick={() => {
                        handleDeleteCustomer(customer.id);
                      }}
                      className="fa fa-trash"
                      aria-hidden="true"
                    ></i>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Modal show={modal} onHide={closeModal}>
          <ModalHeader closeButton>
            <ModalTitle>Add Customer</ModalTitle>
          </ModalHeader>
          <ModalBody>
            <Form>
              <Form.Group className="mb-3">
                <Form.Control
                  value={name}
                  onChange={(e) => {
                    handleChangeCustomerName(e);
                  }}
                  type="text"
                  placeholder="Name"
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Control
                  value={address}
                  onChange={(e) => {
                    handleChangeAddress(e);
                  }}
                  type="text"
                  placeholder="Address"
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Control
                  value={phone_number}
                  onChange={(e) => {
                    handleChangePhoneNumber(e);
                  }}
                  type="text"
                  placeholder="Phone Number"
                />
              </Form.Group>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleCreateCustomer} variant="success">
              Create
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
}

export default Customer;
