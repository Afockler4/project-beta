import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import Manufacturers from "./Cars/Manufacturers";
import Models from "./Cars/Models";
import Reps from "./Sales/Reps";
import Customer from "./Sales/Customer";
import "./index.css";
import SalesRecords from "./Sales/SalesRecords";
import Automobiles from "./Cars/Automobiles";
import TechnicianForm from "./Service/technicianForm";
import ServiceAppointmentForm from "./Service/serviceAppointmentForm";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/Cars/Manufacturers" element={<Manufacturers />} />
          <Route path="/Cars/Models" element={<Models />} />
          <Route path="/Cars/Automobiles" element={<Automobiles />} />
          <Route path="/Sales/Reps" element={<Reps />} />
          <Route path="/Service/Technicians" element={<TechnicianForm />} />
          <Route
            path="/Service/ServiceAppointments"
            element={<ServiceAppointmentForm />}
          />
          <Route path="/Sales/Customer" element={<Customer />} />
          <Route path="/Sales/SalesRecords" element={<SalesRecords />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
