import { NavLink } from "react-router-dom";
import Dropdown from "react-bootstrap/Dropdown";
import Manufacturers from "./Cars/Manufacturers";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          <span className="carcar">CarCar</span>
        </NavLink>
        <Dropdown>
          <Dropdown.Toggle className="salesbar" variant="clear">
            Inventory
          </Dropdown.Toggle>
          <Dropdown.Menu className="salesmenu">
            <Dropdown.Item>
              <NavLink className="navbar-brand" to="/Cars/Manufacturers">
                Manufacturers
              </NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="navbar-brand" to="/Cars/Automobiles">
                Automobiles
              </NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="navbar-brand" to="/Cars/Models">
                Models
              </NavLink>
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        <Dropdown>
          <Dropdown.Toggle className="salesbar" variant="clear">
            Sales
          </Dropdown.Toggle>
          <Dropdown.Menu className="salesmenu">
            <Dropdown.Item>
              <NavLink className="navbar-brand" to="/Sales/Reps">
                Reps
              </NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="navbar-brand" to="/Sales/Customer">
                Customers
              </NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="navbar-brand" to="/Sales/SalesRecords">
                Sales Records
              </NavLink>
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        <Dropdown>
          <Dropdown.Toggle className="salesbar" variant="clear">
            Service
          </Dropdown.Toggle>
          <Dropdown.Menu className="salesmenu">
            <Dropdown.Item>
              <NavLink className="navbar-brand" to="/Service/Technicians">
                Technicians
              </NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="navbar-brand" to="/Service/ServiceAppointments">
                Appointments
              </NavLink>
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0"></ul>
          <ul className="navbar-nav me-auto mb-2 mb-lg-0"></ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
