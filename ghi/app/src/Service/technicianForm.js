import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function TechniciansForm() {
  useEffect(() => {
    getData();
  }, []);

  const [technicians, setTechnicians] = useState([]);

  const [name, setName] = useState("");

  const [employee_number, setEmployeeNumber] = useState();

  const [modal, showModal] = useState(false);

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  const handleCreateTechnician = async () => {
    const data = { name, employee_number };
    const manufacturerNameUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerNameUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
    closeModal();
  };

  const handleChangeName = (e) => {
    setName(e.target.value);
  };

  const handleChangeEmployeeNumber = (e) => {
    setEmployeeNumber(e.target.value);
  };

  const closeModal = () => {
    showModal(false);
  };

  const handleDeleteTechnician = async (id) => {
    // const data = { id }; //not sure what this is doing
    const manufacturerNameUrl = `http://localhost:8080/api/technicians/${id}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerNameUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div className="otherpagescontainer">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Technicians</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Employee Number</th>
              <th>
                <i
                  onClick={() => {
                    showModal(true);
                  }}
                  className="fas fa-plus-square"
                ></i>
              </th>
            </tr>
          </thead>
          <tbody>
            {technicians.map((technicians) => {
              return (
                <tr key={technicians.employee_id}>
                  <td>{technicians.name}</td>
                  <td>{technicians.employee_number}</td>
                  <td>
                    <i
                      onClick={() => {
                        handleDeleteTechnician(technicians.id);
                      }}
                      className="fa fa-trash"
                      aria-hidden="true"
                    ></i>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Modal show={modal} onHide={closeModal}>
          <ModalHeader closeButton>
            <ModalTitle>Add Technician</ModalTitle>
          </ModalHeader>
          <ModalBody>
            <Form>
              <Form.Group className="mb-3">
                <Form.Control
                  value={name}
                  onChange={(e) => {
                    handleChangeName(e);
                  }}
                  type="text"
                  placeholder="Name"
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Control
                  value={employee_number}
                  onChange={(e) => {
                    handleChangeEmployeeNumber(e);
                  }}
                  type="text"
                  placeholder="Employee Number"
                />
              </Form.Group>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleCreateTechnician} variant="success">
              Create
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
}

export default TechniciansForm;
