import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Dropdown from "react-bootstrap/Dropdown";

function Reps() {
  useEffect(() => {
    getData();
  }, []);

  const [appointments, setAppointments] = useState([]);

  const [automobiles, setAutomobiles] = useState([]);

  const [automobile, setAutomobile] = useState("");

  const [status, setStatus] = useState("");

  const [owner_name, setOwnerName] = useState("");

  const [datetime_of_appointment, setDateTime] = useState("");

  const [technician, setTechnician] = useState("");

  const [technicians, setTechnicians] = useState([]);

  const [reason_for_appointment, setReason] = useState("");

  const [modal, showModal] = useState(false);

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }

    const response2 = await fetch("http://localhost:8080/api/technicians/");

    if (response2.ok) {
      const data = await response2.json();
      setTechnicians(data.technicians);
    }

    const response3 = await fetch(
      "http://localhost:8080/api/appointments/autos"
    );

    if (response3.ok) {
      const data = await response3.json();
      setAutomobiles(data.automobileVO);
    }
  };

  const handleCreateAppointment = async () => {
    const data = {
      owner_name,
      datetime_of_appointment,
      reason_for_appointment,
      status,
      technician,
      automobile,
    };
    const AppointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(AppointmentUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
    closeModal();
  };

  const handleChangeAutoVin = (e) => {
    setAutomobile(e.target.value);
  };

  const handleChangeOwnerName = (e) => {
    setOwnerName(e.target.value);
  };

  const handleChangeDateTime = (e) => {
    setDateTime(e.target.value);
  };

  const handleChangeTechnician = (e) => {
    setTechnician(e.target.value);
  };

  const handleChangeReason = (e) => {
    setReason(e.target.value);
  };

  const handleChangeStatus = (e) => {
    setStatus(e.target.value);
  };

  const closeModal = () => {
    showModal(false);
  };

  const handleDeleteAppointment = async (id) => {
    const AppointmentUrl = `http://localhost:8080/api/appointments/${id}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(AppointmentUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div className="otherpagescontainer">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Appointments</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Owner name</th>
              <th>Date</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
              <th>
                <i
                  onClick={() => {
                    showModal(true);
                  }}
                  className="fas fa-plus-square"
                ></i>
              </th>
            </tr>
          </thead>
          <tbody>
            {appointments.map((appointment) => {
              return (
                //there's a key that should go in tr but im not sure what it does: key={appointment.auto_vin}
                <tr>
                  <td>{appointment.automobile.vin}</td>
                  <td>{appointment.owner_name}</td>
                  <td>{appointment.datetime_of_appointment}</td>
                  <td>{appointment.technician.name}</td>
                  <td>{appointment.reason_for_appointment}</td>
                  <td>{appointment.status}</td>
                  <td>
                    <i
                      onClick={() => {
                        handleDeleteAppointment(appointment.id);
                      }}
                      className="fa fa-trash"
                      aria-hidden="true"
                    ></i>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Modal show={modal} onHide={closeModal}>
          <ModalHeader closeButton>
            <ModalTitle>Add an Appointment</ModalTitle>
          </ModalHeader>
          <ModalBody>
            <Form>
              <Form.Group className="mb-3">
                <Form.Control
                  value={owner_name}
                  onChange={(e) => {
                    handleChangeOwnerName(e);
                  }}
                  type="text"
                  placeholder="Owner Name"
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Control
                  value={datetime_of_appointment}
                  onChange={(e) => {
                    handleChangeDateTime(e);
                  }}
                  type="text"
                  placeholder="Date"
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Control
                  value={reason_for_appointment}
                  onChange={(e) => {
                    handleChangeReason(e);
                  }}
                  type="text"
                  placeholder="Reason"
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Control
                  value={status}
                  onChange={(e) => {
                    handleChangeStatus(e);
                  }}
                  type="text"
                  placeholder="Status"
                />
              </Form.Group>
              <Dropdown>
                <Dropdown.Toggle variant="dark">
                  {technician === "" ? "Technician" : technician}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  {technicians.map((technician) => {
                    return (
                      <Dropdown.Item
                        onClick={() =>
                          setTechnician(technician.employee_number)
                        }
                      >
                        {technician.name}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
              <Dropdown>
                <Dropdown.Toggle variant="dark">
                  {automobile === "" ? "VIN" : automobile}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  {automobiles.map((automobile) => {
                    return (
                      <Dropdown.Item
                        onClick={() => setAutomobile(automobile.vin)}
                      >
                        {automobile.vin}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleCreateAppointment} variant="success">
              Create
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
}

export default Reps;
