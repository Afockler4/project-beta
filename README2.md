# CarCar

Team: Andrew Focker and Jaiden Faulkner

- Person 1 - Andrew
- Person 2 - Jaiden

## Design

The overall design of this project was pretty simple, 3 different microservices, where 2 of the microservices interacts with the inventory microservice for various pieces of data.

## Service microservice

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

In my models I have a Technician and Service Appoinment model. The technician model is simpple and just has no foreign keys. The Service Appoinment model is more complicated and has technician (which is in the same microservice) as a foreign key, and also has automobile (which is in the inventory microservice) as a foreign key. I also have an Automobile Value object that I use later in my views.

In my views.py I have a view function for getting a list of technicians and posting a technician that's fully functional. I also have a view function that shows a specific technician and can also delete or update the data (also fully functional).
I also have the same functionality for my appoinments. Two functions, one shows a list and can create an appointment, the other can list a specific one and update or delete it.
All of these view functions use various encoders that are stored in a differenct file and imported into the views.py page.

So for my microservice I mainly interact with the inventory microservice to get automobile data.
